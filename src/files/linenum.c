#include <stdio.h>
//Rachel
int main(int argc, char* argv[]) {
  if (argc <= 1) {
    fprintf(stdout,"ERROR: Missing filename\nExample: %s filename.txt\n\n",argv[0]);
    return 1;
  }
  FILE* infile = fopen(argv[1],"r");
  char line[400];
  size_t i = 0;
  while (fgets(line, sizeof(line), infile)) { 
     printf("%4d: %s", ++i, line);
  }
  return 0;
}

