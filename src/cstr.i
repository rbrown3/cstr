%module cstr
%{
#include <stdint.h>
#include <stdbool.h>
typedef struct cstr_struct {
   uint32_t    len;
   uint32_t    size;
   uint32_t    max_size;
   char*       str;
   struct cstr_struct* next;
} cstr_t;

extern cstr_t* cstr_new(const uint32_t max_size, const char* init_str);
extern int cstr_copy(char* dest, const char* src, const uint32_t size);
extern int cstr_resize(cstr_t* cstr, const uint32_t new_size);
extern int cstr_append(cstr_t* orig, const char* str);
extern cstr_t* cstr_free(cstr_t* str);
extern uint32_t cstr_get_filesize(const char* const filename);
extern cstr_t* cstr_read(const char* const filename);
extern void cstr_print(const cstr_t* const cstring);

extern void cstr_first(cstr_t* cstring);
extern bool cstr_valid(cstr_t* cstring);
extern void cstr_next(cstr_t* cstring);
extern char cstr_get(cstr_t* cstring);
%}

#include <stdint.h>
#include <stdbool.h>

typedef struct cstr_struct {
   uint32_t    len;
   uint32_t    size;
   uint32_t    max_size;
   char*       str;
   struct cstr_struct* next;
} cstr_t;

extern cstr_t* cstr_new(const uint32_t max_size, const char* init_str);
extern int cstr_copy(char* dest, const char* src, const uint32_t size);
extern int cstr_resize(cstr_t* cstr, const uint32_t new_size);
extern int cstr_append(cstr_t* orig, const char* str);
extern cstr_t* cstr_free(cstr_t* str);
extern uint32_t cstr_get_filesize(const char* const filename);
extern cstr_t* cstr_read(const char* const filename);
extern void cstr_print(cstr_t* cstring);

extern void cstr_first(cstr_t* cstring);
extern bool cstr_valid(cstr_t* cstring);
extern void cstr_next(cstr_t* cstring);
extern char cstr_get(cstr_t* cstring);

