/*
MIT License

Copyright (c) 2018 JWRR.COM

git clone https://github.com/jwrr/lued.git

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/// @file
/// @brief Header file for cstr.h
///
/// @main

#ifndef CSTR_H
#define CSTR_H

#include <stdint.h>

typedef struct cstr_struct {
   uint32_t    len;
   uint32_t    size;
   uint32_t    elem_size;
   uint32_t    max_size;
   uint32_t    it;
   char*       str;
   struct cstr_struct* next;
} cstr_t;

cstr_t* cstr_new(const uint32_t max_size, const char* init_str);

int cstr_copy(char* dest, const char* src, const uint32_t size);

int cstr_resize(cstr_t* cstr, const uint32_t new_size);

int cstr_append(cstr_t* orig, const char* str);

cstr_t* cstr_free(cstr_t* str);

uint32_t cstr_get_filesize(const char* const filename);

cstr_t* cstr_read(const char* const filename);
void cstr_print(cstr_t* cstring);

void cstr_firsti(cstr_t* cstring);
bool cstr_validi(cstr_t* cstring);
void cstr_nexti(cstr_t* cstring);
char cstr_geti(cstr_t* cstring);
void cstr_seti(cstr_t* cstring, char ch);
uint32_t cstr_i(cstr_t* cstring);

uint32_t cstr_count(cstr_t* haystack, char* needles);
// deleteme cstrarr_t* cstr_split(cstr_t* cstring, char* delims);


#endif
