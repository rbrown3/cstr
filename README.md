cstr
====

Description
-----------
A dynamic string library for the C programming language.


Documentation
-------------
* [View Documentation](https://rbrown3.bitbucket.io/cstr/doxygen/cstr_8c.html)
* [View Code Coverage](https://rbrown3.bitbucket.io/cstr/lcov/src/index.html)
* [View Memory Leak Report](https://rbrown3.bitbucket.io/cstr/valgrind/valgrind.xml)

Git Notes
---------
```bash
# Git Commands for initial commit/push
git init
ls -a  # see that .git folder was created
git status
git add src/*.c
git add src/*.h
git status
vi README.md
git add README.md
git status
git commit -m 'Initial'
git config --global push.default simple
git remote add origin https://rbrown3@bitbucket.org/rbrown3/cstr.git
git push --set-upstream origin master
```
```bash
git clone https://rbrown3@bitbucket.org/rbrown3/cstr.git
git pull # resync to master
```

```bash
# Remember git password for 1 hour
git config credential.helper cache 3600

# Remember git password forever
git config credential.helper store

```

```bash
# Git Commands after initial commit/push
vi README.md
git add README.md
git status
git commit -m 'Add project description to README.md'
git push
```

Doxygen Notes
-------------

```bash
# Create Doxyfile
doxygen -g. # Generates Doxyfile
vi Doxyfile
- change 'INPUT =' to 'INPUT = src'
```

```bash
# Generate Doxygen Documentation
doxygen Doxyfile
```

```bash
# At the top of every src file

/// @file
/// @brief This file provides blah
///
/// @main Optional Main stuff
```

```bash
# Before every function
/// @brief Blah Blah
/// More Blah Blah Compare t, often over multiple lines for detail
/// description of the function.
/// @param param_name parameter description
/// @return description of return value.
 **/
```

```bash
# Add Requirements (\req) tag to Doxyfile

ALIASES += "req=\xrefitem req \"Requirement\" \"Requirements\""
```

GDB Notes
---------

```bash
gcc -g *.c # compile with -g flag to dump
gdb a.out
br cstr.c:86 # set breakpoint
r # run
n  # next skips over function call
s  # step goes into function call
fin # finish, run to end of function
tb 92 # temporary breakpoint
c  # continue
print value
print *ptr
disp value # auto display value after every stop
delete disp # remove auto displayed values
info lo # local variables
info br # breakpoints
delete # delete all breakpoints
l # list lines of source code (l 42; l file.c:42)

bt # shows call history of where program is (same as backtrace, where, info stack)
up # go up one level in call stack
ctrl-x a # toggle in and out of tui mode
file executablefile # reload
quit

```

```bash
#Quick way to find segmentation fault
gdb executable-name
run # The program should stop at seg fault
bt # This shows where the program crashed and call history
q # quit / exit gdb
```

TCL and SWIG Notes
------------------
I am going to extend TCL with cstr functions.

Here is the [http://swig.org/tutorial.html](Swig tutorial).

```bash
# Install TCL and Swig
sudo apt-get update # I also needed to do sudo apt-get dist-upgrade
sudo apt-get install tcl
sudo apt-get install tcl-dev
sudo apt-get install swig
```

```bash
#
vi cstr.i
vi example.c
vi example.i
swig -tcl example.i
ls
gcc -fpic -c example.c example_wrap.c -I/usr/local/include
sudo find / |grep tcl.h
gcc -fpic -c example.c example_wrap.c -I/usr/include/tcl8.6
gcc -shared example.o example_wrap.o -o example.so
tclsh
```

LUA NOTES
---------
Call Lua scripts from C

```bash
sudo apt-get update
sudo apt-get install lua5.2
sudo apt-get install lua5.2-dev

gcc -I/usr/include/lua5.2 first_lua.c -llua5.2: -lm
```

```lua
print("Hello")
```

```c
# first_lua.c
#include <stdio.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

int prompt(char* question, char* response, size_t size)
{
   printf("%s: ",question);
   int valid = (fgets(response, size, stdin) != NULL);
   if (!valid) return 0;

   int i;
   for (i=0; response[i]!='\0' && i < size-1; i++) {
      if (response[i] == '\n') break;
   }
   response[i] = '\0';
   return (i > 0);
}

int main (void) {
  char buff[256];
  int error;
  lua_State *L = lua_open();   /* opens Lua */
  luaL_openlibs(L);

//  luaopen_base(L);             /* opens the basic library */
//  luaopen_table(L);            /* opens the table library */
//  luaopen_io(L);               /* opens the I/O library */
//  luaopen_string(L);           /* opens the string lib. */
//  luaopen_math(L);             /* opens the math lib. */

  luaL_dostring(L, "print('hello from string version ='.._VERSION)");
  luaL_dofile (L, "hello.lua");

  while (prompt("Enter command", buff, sizeof(buff))) {
    strncat(buff, ".lua", sizeof(buff));
    printf("calling '%s'\n", buff);
    luaL_dofile(L, buff);
  }

  lua_close(L);
  return 0;
}
```

Random Notes
------------

```bash
#Semi automate creating c header file

grep -B1 ^{ carr.c |grep -v '^[-{]' |sed 's#$#;#'

```

```bash
# In sed regex, escape parenthese for grouping
# The following removes trailing semicolon on lines with EQ

sed 's#EQ\(.*\);#EQ\1#' carr_test.c
```




